=====================
Mini-Project-1
=====================
---------------------
Summary
---------------------
This is the first mini-project that will be created.  The purpose of said project is to prepare for the
semester's final project.
---------------------
Project Goals:
---------------------
1. Create a Lua script that does an action.
2. Write a C++ program that can call the Lua script.
3. Sucessfully change the Lua script while the C++ program runs.
