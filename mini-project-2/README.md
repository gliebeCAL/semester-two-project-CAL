=====================
Mini Project 2
=====================
---------------------
Summary
---------------------
This is the second mini project I am working on for the project.  The goal of this project is to learn how to
intergrate Lua into a C++ program.  This project must be properly commented, and major progress points should
recorded in **documentation.txt**.
---------------------
Project Goals:
---------------------
1. Create a C++ Program that can use multiple Lua scripts to extend the use of the program in some way
